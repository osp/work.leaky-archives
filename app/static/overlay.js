function querySelectParent (el, selector) {
  if (el.parentNode) {
    if (el.parentNode.matches(selector)) {
      return el.parentNode;
    }
    else {
      return querySelectParent(el.parentNode, selector);
    }
  }

  return null;
}

// function selectInOrder(selectors, parent) {
//   return selectors.flatMap((selector) => parent.querySelectorAll(selector))
// }

// function cloneNodes (nodes) {
//   return nodes.filter(node => node ? true : false).map(node => node.cloneNode(true));
// }

function selectAndClone (selector, parent) {
  return parent.querySelector(selector)?.cloneNode(true);
}

function getTemplate (name) {
  return document.querySelector(`template[name=${name}]`).content.firstElementChild.cloneNode(true);
}

function initOverlay(contentNodes, headerNodes, handler) {
  const overlay = getTemplate('overlay'),
        overlayCloseButton = overlay.querySelector('#overlay--close'),
        overlayHeader = overlay.querySelector('#overlay--header'),
        overlayContentContainer = overlay.querySelector('#overlay--content-container');

  document.body.dataset.hasOverlay = true;

  function closeOverlay () {
    overlay.remove();
    delete document.body.dataset.hasOverlay;
  }

  if (headerNodes) {
    for (let i = 0; i < headerNodes.length; i++) {
      overlayHeader.insertBefore(headerNodes[i], overlayCloseButton);
    }
  }

  if (contentNodes) {
    for (let i = 0; i < contentNodes.length; i++) {
      if (contentNodes[i]) {
        overlayContentContainer.appendChild(contentNodes[i]);
      }
    }
      
  }

  document.body.appendChild(overlay);

  overlayCloseButton.addEventListener('click', closeOverlay);

  overlay.addEventListener('click', function (e) {
    if (e.target == this) {
      closeOverlay();
    }
  });

  function closeHandler (e) {
    if (e.code === 'Escape') {
      e.preventDefault();
      closeOverlay();
      document.removeEventListener('keydown', closeHandler);
    } 
  }

  document.addEventListener('keydown', closeHandler);

  if (handler) {
    handler(overlay);
  }
}

const textPreviews = document.querySelectorAll('[data-type="text"]');

for (let i = 0; i < textPreviews.length; i++) {
  textPreviews[i].addEventListener('click', function () {
    const headerNodes = [
            selectAndClone('.file--header', this),
            selectAndClone('.file--meta', this)
          ],
          contentNodes = [ selectAndClone('.file-preview--text', this) ];

    initOverlay(contentNodes, headerNodes);
  });
}

const imagePreviews = document.querySelectorAll('article[data-type="image"]');

for (let i = 0; i < imagePreviews.length; i++) {
  imagePreviews[i].querySelector('[data-thumbnail-link]').addEventListener('click', function (e) { 
    e.preventDefault(); });
  imagePreviews[i].addEventListener('click', function () {
    const headerNodes = [
            selectAndClone('.file--header', this),
            selectAndClone('.file--meta', this)
          ];
          img = selectAndClone('.file--preview--image', this),
          caption = selectAndClone('.fileCaption', this);

    img.src = img.dataset.srcPreview;

    initOverlay([ img, caption ], headerNodes);
  });
}

const PDFPreviews = document.querySelectorAll('article[data-type="pdf"]');

for (let i = 0; i < PDFPreviews.length; i++) {
  PDFPreviews[i].addEventListener('click', function (e) { 
    e.preventDefault();
    const objectNode = document.createElement('object');
    objectNode.setAttribute('data', this.querySelector('[data-pdf-thumbnail]').getAttribute('href'));
    objectNode.setAttribute('type', 'application/pdf');
    objectNode.classList.add('pdf');
    initOverlay([objectNode], [ selectAndClone('.file--header', this), selectAndClone('.file--meta', this) ]);
  });
}


const VideoPreviews = document.querySelectorAll('article[data-type="video"]');

for (let i = 0; i < VideoPreviews.length; i++) {
  VideoPreviews[i].addEventListener('click', function (e) { 
    e.preventDefault();
    const videoNode = document.createElement('video'),
          sourceNode = document.createElement('source');
    videoNode.setAttribute('controls', 'controls');
    sourceNode.setAttribute('src', this.dataset.videoSrc);
    sourceNode.setAttribute('type', this.dataset.mime);
    videoNode.appendChild(sourceNode);
    initOverlay([videoNode], [ selectAndClone('.file--header', this), selectAndClone('.file--meta', this) ]);
  });
}