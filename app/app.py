from flask import Flask, abort, render_template, send_file, redirect, url_for
from werkzeug.security import safe_join
import os
import magic
import pypandoc
from PIL import Image
from bs4 import BeautifulSoup
from slugify import slugify
import subprocess
import glob
import threading
import collections
import datetime

Image.MAX_IMAGE_PIXELS = 178956970 * 2

try:
  from settings import CAPTION_JOIN_TOKEN, CAPTION_TOKEN, DOC_MEDIA_FOLDER, DOC_PREVIEW_FOLDER, DOC_MEDIA_PUBLIC_BASE_PATH, EXCLUDE_FOLDER_FROM_HOMEPAGE_TOKEN, ILLUSTRATION_TOKEN, NEXTCLOUD_FOLDER, PDF_PREVIEW_FOLDER, PDF_PREVIEW_PUBLIC_BASE_PATH, PDF_PREVIEW_RESOLUTION, PUBLIC_BASE_PATH, README_TOKEN, ROLLOVER_TOKEN, SITEURL, STATICURL, TAGS_TOKEN, THUMBNAIL_PUBLIC_BASE_PATH, THUMBNAIL_FOLDER, VIDEO_PREVIEW_FOLDER, VIDEO_PREVIEW_PUBLIC_BASE_PATH, THUMBNAIL_SIZE_SMALL, THUMBNAIL_SIZE_BIG
except ImportError:
  from .settings import CAPTION_JOIN_TOKEN, CAPTION_TOKEN, DOC_MEDIA_FOLDER, DOC_PREVIEW_FOLDER, DOC_MEDIA_PUBLIC_BASE_PATH, EXCLUDE_FOLDER_FROM_HOMEPAGE_TOKEN, ILLUSTRATION_TOKEN, NEXTCLOUD_FOLDER, PDF_PREVIEW_FOLDER, PDF_PREVIEW_PUBLIC_BASE_PATH, PDF_PREVIEW_RESOLUTION, PUBLIC_BASE_PATH, README_TOKEN, ROLLOVER_TOKEN, SITEURL, STATICURL, TAGS_TOKEN, THUMBNAIL_PUBLIC_BASE_PATH, THUMBNAIL_FOLDER, VIDEO_PREVIEW_FOLDER, VIDEO_PREVIEW_PUBLIC_BASE_PATH, THUMBNAIL_SIZE_SMALL, THUMBNAIL_SIZE_BIG

# Verifies given path exists and is newer than given
# modification time
def path_exists_and_newer (cache_path, modification_time):
  if os.path.exists(cache_path):
     statinfo = os.stat(cache_path)
     return statinfo.st_mtime > modification_time.timestamp()

  return False

import cv2

def make_video_thumbnail (video_path, thumbnail_path):
  dirname = os.path.dirname(thumbnail_path)
      
  if not os.path.exists(dirname):
    os.makedirs(dirname)

  _, image = cv2.VideoCapture(video_path).read()
  height, width, _ = image.shape
  if width > height:
    r = THUMBNAIL_SIZE_SMALL[0] / width
  else:
    r = THUMBNAIL_SIZE_SMALL[1] / height
  thumbnail = cv2.resize(image, (int(width * r), int(height * r)), interpolation=cv2.INTER_LANCZOS4)
  cv2.imwrite(thumbnail_path, thumbnail)

import os
import shutil
import hashlib
import tempfile

def make_hashed_basename (basename):
  return hashlib.sha224(basename.encode()).hexdigest()

def make_pdf_thumbnail (pdf_path, thumbnail_path):
  dirname = os.path.dirname(thumbnail_path)
      
  if not os.path.exists(dirname):
    os.makedirs(dirname)

  # Ghostscript seems to break when there are double quotes in the filename.
  # First make a link, with a safe filename, then generate the thumbnail
  # Thumbnail with a safename as well, to then move it to an unsafe path

  with tempfile.TemporaryDirectory(prefix='indexer-pdf-preview__') as tmpdir:
    basename = os.path.basename(pdf_path)
    basename_safe = make_hashed_basename(basename)
    pdf_safe_path = os.path.join(tmpdir, basename_safe)
    # Safe name for the thumbnail
    thumbnail_safe_path = '{}.jpg'.format(pdf_safe_path)

    # Make link to original PDF with 'safe name'
    os.symlink(pdf_path, pdf_safe_path)

    args = [ 'gs', '-sPageList=1', '-sDEVICE=jpeg', '-dAutoRotatePages=/None', '-r{}'.format(PDF_PREVIEW_RESOLUTION), '-o', thumbnail_safe_path, pdf_safe_path ]

    try:
      subprocess.check_output(args, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
      result = 'Error:\n{}'.format(e.output.decode())
      print(result)

    # Remove created link
    os.remove(pdf_safe_path)

    if os.path.exists(thumbnail_safe_path):
      # Move thumbnail from the safe name to where we expect it.
      shutil.move(thumbnail_safe_path, thumbnail_path)

# Todo Parent directory as a property, also on File classes
class Directory (object):
  
  typestring = 'Directory'

  def __init__ (self, path, public_path, entries, caption, tags=[], illustration=None, rollover=None, on_homepage=False):
    self.path = path
    self.basename = os.path.basename(path)
    self.public_path = public_path
    self.root = False
    self.caption = caption
    # self.entries = sorted(entries, key=lambda e: e.modification_time, reverse=True)
    # self.entries = sorted(entries, key=lambda e: str(e).lower())
    self.entries = entries
    self.tags = tags
    self.illustration = illustration
    self.rollover = rollover
    self.on_homepage = on_homepage
    self.visible = False if self.basename.startswith('.') else True

    statinfo = os.stat(self.path)

    self.creation_time = datetime.datetime.fromtimestamp(statinfo.st_ctime)
    self._modification_time = datetime.datetime.fromtimestamp(statinfo.st_mtime)

  def __str__ (self):
    return "{} ({})".format(self.basename, ', '.join(map(str, self.entries)))

  def __len__ (self):
    return len(self.entries.keys())
  
  def __iter__ (self):
    return iter(sorted(self.entries.values(), key=lambda entry: str(entry).lower()))

  def unfold (self):
    for entry in self.entries.values():
      yield entry
      if isinstance(entry, Directory):
        print('folder')
        for dir_entry in entry.unfold():
          yield dir_entry

  @property
  def statistics (self):
    return collections.Counter(map(lambda e: e.typestring, self.unfold()))

  @property
  def entry_count_summed (self):
    entry_count = 0
    file_count = 0
    dir_count = 0

    for entry in self.entries.values():
      entry_count += 1

      if is_dir(entry):
        dir_count += 1
        dir_entry_count = entry.entry_count_summed
        entry_count += dir_entry_count['entries']
        file_count += dir_entry_count['files']
        dir_count += dir_entry_count['directories']

      else:
        file_count += 1

    return {
      'entries': entry_count,
      'files': file_count,
      'directories': dir_count
    }

  @property
  def entry_count (self):
    entry_count = len(self.entries)
    directory_count = len(list(filter(is_dir, self.entries.values())))

    return {
      'entries': entry_count,
      'files': entry_count - directory_count,
      'directories': directory_count
    }
  
  @property
  def preview (self):
    return self.entry_count

  @property
  def modification_time (self):
    if self.entries:
      return max([entry.modification_time for entry in self.entries])
    else:
      return self._modification_time


class File (object):
  mimetypes = []
  extensions = []

  typestring = 'File'

  def __init__ (self, path, public_path, mime):
    self.path = path
    self.basename = os.path.basename(path)
    self.public_path = public_path
    self.caption = None
    self.mime = mime
    self.visible = False if self.basename.startswith('.') else True

    statinfo = os.stat(self.path)

    self.size = statinfo.st_size
    self.creation_time = datetime.datetime.fromtimestamp(statinfo.st_ctime)
    self.modification_time = datetime.datetime.fromtimestamp(statinfo.st_mtime)

  def __str__ (self):
    return self.basename

  @property
  def preview (self):
    pass

class TextFile (File):
  mimetypes = [
    'text/plain',
    'text/markdown',
    'text/html',
    'application/rtf',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.oasis.opendocument.text',
  ],

  typestring = 'Text file'

  extensions = ['.rtf', '.txt', '.docx', '.html', '.md']
  
  @property
  def preview (self):
    cache_path = os.path.join(DOC_PREVIEW_FOLDER, '{}.html'.format(self.public_path))
    media_dirname, _ = os.path.splitext(self.public_path)
    media_path = os.path.join(DOC_MEDIA_FOLDER, media_dirname)
    public_media_path = os.path.join(DOC_MEDIA_PUBLIC_BASE_PATH, media_dirname)

    if not path_exists_and_newer(cache_path, self.modification_time):
      dirname = os.path.dirname(cache_path)
      
      if not os.path.exists(dirname):
        os.makedirs(dirname)


      pypandoc.convert_file(glob.escape(self.path), 'html', outputfile=cache_path, extra_args=['--extract-media', media_path, '--resource-path', os.path.dirname(self.path)])

      # Reopen generated HTML to adjust image paths: now they are absolute
      # but should use the internet accessible addresses
      with open(cache_path, 'r') as fp:
        soup = BeautifulSoup(fp, 'html.parser')
        for img in soup.find_all('img'):
          src = img.get('src')
          if src and src.startswith(media_path):
            img['data-src'] = src.replace(media_path, public_media_path)
            del img['src']
            

      with open(cache_path, 'w') as fp:
        fp.write(str(soup))
      
    return open(cache_path, 'r').read()


class ImageFile (File):
  mimetypes = [
    'image/jpeg',
    'image/png',
    'image/gif'
  ]

  typestring = 'Image'

  def make_thumbnail (self, size, name):
    thumbnail_path = os.path.join(THUMBNAIL_FOLDER, name, self.public_path)

    if not path_exists_and_newer(thumbnail_path, self.modification_time):
      dirname = os.path.dirname(thumbnail_path)

      if not os.path.exists(dirname):
        os.makedirs(dirname)

      try:
        with Image.open(self.path) as im:
          im.thumbnail(size)
          im.save(thumbnail_path, q=75)
      except Image.DecompressionBombError:
        # Better resolving of DecompressionBomb
        return None

    return os.path.join(THUMBNAIL_PUBLIC_BASE_PATH, name, self.public_path)

  @property
  def preview (self):
    return self.make_thumbnail(THUMBNAIL_SIZE_SMALL, 'thumb')
  
  @property
  def preview_big (self):
    return self.make_thumbnail(THUMBNAIL_SIZE_BIG, 'preview')


class VideoFile (File):
  mimetypes = [
    'video/ogg',
    'video/avi',
    'video/mp4',
    'video/x-matroska',
    'video/avi',
    'video/msvideo',
    'video/x-msvideo',
    'video/3gpp',
  ],


  typestring = 'Video file'

  extensions = ['.mp4', '.m4v', '.mkv', '.mov', '.avi', '.ogv', '.ogg', '.3gp', '.3g2']

  @property
  def preview (self):
    thumbnail_path =  os.path.join(VIDEO_PREVIEW_FOLDER, '{}.jpg'.format(self.public_path))
    public_media_path = os.path.join(VIDEO_PREVIEW_PUBLIC_BASE_PATH, '{}.jpg'.format(self.public_path))

    if not path_exists_and_newer(thumbnail_path, self.modification_time):
      make_video_thumbnail(self.path, thumbnail_path)

    return public_media_path
  
class PDFFile (File):
  mimetypes = ['application/pdf']

  typestring = 'PDF file'

  @property
  def preview (self):
    thumbnail_path = os.path.join(PDF_PREVIEW_FOLDER, '{}.jpg'.format(self.public_path))
    public_media_path = os.path.join(PDF_PREVIEW_PUBLIC_BASE_PATH, '{}.jpg'.format(self.public_path))

    if not path_exists_and_newer(thumbnail_path, self.modification_time):
      make_pdf_thumbnail(self.path, thumbnail_path)
      
      
    return public_media_path

class YoutubeFile (File):
  extensions = ['.youtube']
  
  typestring = 'Youtube video'

  @property
  def preview (self):
    return os.path.splitext(self.basename)[0]

class VimeoFile (File):
  extensions = ['.vimeo']
  typestring = 'Vimeo video'

  @property
  def preview (self):
    return os.path.splitext(self.basename)[0]

class AudioFile (File):
  mimetypes = [
    'audio/mpeg',
    'audio/MPA',
    'audio/mpa-robust',
    'application/ogg', 
    'audio/ogg',
    'audio/vorbis',
    'audio/vorbis-config',
    'audio/m4a',
    'audio/vnd.wave',
    'audio/wav',
    'audio/wave',
    'audio/x-wav'
  ],

  typestring = 'Audio file'

  extensions = ['.mp3', '.ogg', '.wav', '.m4a']

  @property
  def preview (self):
    pass

class MediaFile (File):
  @property
  def preview (self): 
    pass


def instantiateFile (path, public_path):
  mime = magic.from_file(path, mime=True)
  _, ext = os.path.splitext(path)

  for candidate in [ImageFile, MediaFile, TextFile, PDFFile, YoutubeFile, VimeoFile, AudioFile, VideoFile]:
    # Loop through possible file types, if detected mime type is linked to the file,
    # or its extension, instantiate the class.
    if mime in candidate.mimetypes or ext in candidate.extensions:
      return candidate(path, public_path, mime)

  # Unknown file, instantiate generic file
  return File(path, public_path, mime)


app = Flask(__name__)


def parsetags (path):
  if os.path.exists(path):
    with open(path, 'r') as h:
      # Strip lines and reject emtpy lines
      tags = list(filter(lambda l: l != '', map(lambda l: l.strip().lower(), h.readlines())))
      return tags
  return []


def parseentry (path, public_path):
  if os.path.isdir(path):
    entries = { filename: parseentry(os.path.join(path, filename), os.path.join(public_path, filename)) for filename in os.listdir(path) }
    
    fileroot_map = { os.path.splitext(filename)[0]: filename for filename in entries.keys() }
    # create a mapping from fileroo to filenames

    fileroots = sorted(fileroot_map.keys(), reverse=True)
    # Reverse sorted, caption files will always be seen before the file
    # it is captioning

    caption = None
    tags = None
    illustration = None
    rollover = None
    on_homepage = True

    for root in fileroots:
      filename = fileroot_map[root]
      if (root.lower() == CAPTION_TOKEN.lower() or root.lower() == README_TOKEN.lower()) and caption is None:
        caption = entries[filename]
        del entries[filename]
      elif root.endswith(CAPTION_JOIN_TOKEN + CAPTION_TOKEN):
        captioned_root = root[:-1 * len(CAPTION_JOIN_TOKEN + CAPTION_TOKEN)]

        if captioned_root in fileroot_map:
          captioned = fileroot_map[captioned_root]
          entries[captioned].caption = entries[filename]

          del entries[filename]
      elif root.lower() == TAGS_TOKEN:
        tags = parsetags(os.path.join(path, filename))
        del entries[filename]
      elif root.lower() == ILLUSTRATION_TOKEN:
        image = instantiateFile(os.path.join(path, filename), os.path.join(public_path, filename))
        illustration = image.preview
        del entries[filename]
      elif root.lower() == ROLLOVER_TOKEN:
        image = instantiateFile(os.path.join(path, filename), os.path.join(public_path, filename))
        rollover = image.preview
        del entries[filename]
      elif root.lower() == EXCLUDE_FOLDER_FROM_HOMEPAGE_TOKEN:
        on_homepage = False

    return Directory(path, public_path, entries=entries, caption=caption, tags=tags, illustration=illustration, rollover=rollover, on_homepage=on_homepage)
  else:
    return instantiateFile(path, public_path)

def get_header (tree):
  document = find_in_tree(tree, ['template-snippets', 'header.html'])
  return document.preview

def get_footer (tree):
  document = find_in_tree(tree, ['template-snippets', 'footer.html'])
  return document.preview


@app.template_filter("standardize_filesize")
def standardize_filesize (size):
  extensions = ['kb', 'mb', 'gb', 'tb', 'pb']
  extension = 'b'

  while size > 1024:
    size = size / 1024
    extension = extensions.pop(0)

  return '{:.2f}{}'.format(size, extension)

@app.template_filter("slugify")
def slugify_string (raw):
  return slugify(raw)

@app.template_test("directory")
def is_dir (candidate):
  return isinstance(candidate, Directory)

@app.template_test("file")
def is_file (candidate):
  return isinstance(candidate, File)

@app.template_test("video_file")
def is_video_file (candidate):
  return isinstance(candidate, VideoFile)

@app.template_test("audio_file")
def is_audio_file (candidate):
  return isinstance(candidate, AudioFile)

@app.template_test("text_file")
def is_text_file (candidate):
  return isinstance(candidate, TextFile)

@app.template_test("image_file")
def is_image_file (candidate):
  return isinstance(candidate, ImageFile)

@app.template_test("media_file")
def is_media_file (candidate):
  return isinstance(candidate, MediaFile)

@app.template_test("pdf_file")
def is_pdf_file (candidate):
  return isinstance(candidate, PDFFile)

@app.template_test("youtube_file")
def is_youtube_file (candidate):
  return isinstance(candidate, YoutubeFile)

@app.template_test("vimeo_file")
def is_vimeo_file (candidate):
  return isinstance(candidate, VimeoFile)

def find_in_tree(tree, path_parts):
  if path_parts[0] in tree.entries:
    if len(path_parts) > 1:
      if is_dir(tree.entries[path_parts[0]]):
        return find_in_tree(tree.entries[path_parts[0]], path_parts[1:])
      else:
        return None
    else:
      return tree.entries[path_parts[0]]
  else:
    return None
  
@app.route("/<path:subpath>")
def file (subpath=None):
  entry = find_in_tree(tree, subpath.split('/'))

  if entry is None:
    abort(404)


  if is_dir(entry):
    return listdir(entry)
  else:
    path = safe_join(NEXTCLOUD_FOLDER, subpath)
    return send_file(path)

def dir_has_tag (directory, tag):
  if directory.tags is not None:
    try:
      directory.tags.index(tag)
      return True
    except ValueError:
      pass
  return False

@app.route("/tag/<string:tag>")
def list_tag (tag):
  print(tree.entries.values(), tag)
  dirs = list(filter(lambda e: is_dir(e) and dir_has_tag(e, tag) and e.visible and e.on_homepage, tree.entries.values()))
  
  if len(dirs) > 0:
    return render_template('list_tag.html', tag=tag, directories=dirs, STATICURL=STATICURL, SITEURL=SITEURL, HEADER_SNIPPET=get_header(tree), FOOTER_SNIPPET=get_footer(tree))
  else:
    return redirect(url_for('index'))

@app.route("/")
def index ():
  # List folders in the root folder.

  # directory = parseentry(NEXTCLOUD_FOLDER, '')
  # directory.root = True # Used in the template to open the directory by default

  return render_template("index.html", directory=tree, STATICURL=STATICURL, SITEURL=SITEURL, HEADER_SNIPPET=get_header(tree), FOOTER_SNIPPET=get_footer(tree))

@app.route("/dir-statistics")
def statistics ():
  return render_template("statistics.html", root=tree)

def listdir(directory):
  return render_template('listing.html', directory=directory, STATICURL=STATICURL, SITEURL=SITEURL, HEADER_SNIPPET=get_header(tree), FOOTER_SNIPPET=get_footer(tree))
  

def filelist (path, path_public):
  # if subpath:
  #   path = safe_join(NEXTCLOUD_FOLDER, subpath)
  #   public_path = os.path.join(PUBLIC_BASE_PATH, subpath)
  # else:
  # path = NEXTCLOUD_FOLDER

  public_path = os.path.join(PUBLIC_BASE_PATH, path_public)

  if not os.path.exists(path):
    abort(404)
  
  if not os.path.isdir(path):
    return send_file(path)

  directory = parseentry(path, public_path)
  directory.root = True # Used in the template to open the directory by default
  
  return render_template('listing.html', directory=directory, STATICURL=STATICURL, SITEURL=SITEURL)


@app.route(os.path.join(DOC_MEDIA_PUBLIC_BASE_PATH, "<path:subpath>"))
def media_thumbnail (subpath=None):
  return send_file(os.path.join(DOC_MEDIA_FOLDER, subpath))


@app.route(os.path.join(THUMBNAIL_PUBLIC_BASE_PATH, "<path:subpath>"))
def thumbnail (subpath=None):
  return send_file(os.path.join(THUMBNAIL_FOLDER, subpath))


@app.route(os.path.join(PDF_PREVIEW_PUBLIC_BASE_PATH, "<path:subpath>"))
def pdf (subpath=None):
  return send_file(os.path.join(PDF_PREVIEW_FOLDER, subpath))


@app.route(os.path.join(VIDEO_PREVIEW_PUBLIC_BASE_PATH, "<path:subpath>"))
def video (subpath=None):
  return send_file(os.path.join(VIDEO_PREVIEW_FOLDER, subpath))



import time, sys, random

def update_tree():
  global tree
  while True:
    time.sleep(50 + random.randint(0, 20))
    sys.stdout.write('Updating tree\n')
    sys.stdout.flush()
    tree = parseentry(NEXTCLOUD_FOLDER, '')

tree = parseentry(NEXTCLOUD_FOLDER, '')
tree.root = True

update_tree_thread = threading.Thread(target=update_tree, daemon=True)
update_tree_thread.start()

# print(tree.statistics)