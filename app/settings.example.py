import os.path

BASE_PATH = os.path.dirname(os.path.realpath(__file__))
NEXTCLOUD_FOLDER = os.path.abspath(os.path.join(BASE_PATH, '..', 'data'))
CACHE_FOLDER = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'cache'))
THUMBNAIL_FOLDER = os.path.join(CACHE_FOLDER, 'thumbnails')
PDF_PREVIEW_FOLDER = os.path.join(CACHE_FOLDER, 'pdf_preview')
VIDEO_PREVIEW_FOLDER = os.path.join(CACHE_FOLDER, 'video_preview')
DOC_PREVIEW_FOLDER = os.path.join(CACHE_FOLDER, 'doc_preview')
DOC_MEDIA_FOLDER = os.path.join(CACHE_FOLDER, 'doc_media')
PUBLIC_BASE_PATH = ''
THUMBNAIL_PUBLIC_BASE_PATH = '/thumbnails/' # should start with /
DOC_MEDIA_PUBLIC_BASE_PATH = '/document_thumbnails/' # should start with
PDF_PREVIEW_PUBLIC_BASE_PATH = '/pdf/' # should start with /
PDF_PREVIEW_RESOLUTION = 96
VIDEO_PREVIEW_PUBLIC_BASE_PATH = '/video/'

CAPTION_TOKEN = 'caption'
CAPTION_JOIN_TOKEN = '-'

README_TOKEN = 'readme'
TAGS_TOKEN = 'tags'
ILLUSTRATION_TOKEN = 'portrait'
ROLLOVER_TOKEN = 'rollover'
HIDE_FOLDER_TOKEN = '.exclude'

SITEURL = ''
STATICURL = os.path.join(SITEURL, 'static')